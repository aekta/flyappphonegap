//Event class for notifications
function Event(sender) {
    this._sender = sender;
    this._listeners = [];
}

Event.prototype = {
    attach : function (listener) {
        this._listeners.push(listener);
    },
    notify : function (args) {
        var index;

        for (index = 0; index < this._listeners.length; index += 1) {
            this._listeners[index](this._sender, args);
        }
    }
};

//Model For Airport  List

function AirportList(){
    
    this._airports = new Array();
    
    this._selectedIndex = -1;
    
    this.itemAdded = new Event(this);
}

AirportList.prototype = {
    getItems : function () {
        return [].concat(this._airports);
    },

    addItem : function (item) {
        this._airports.push(item);
        this.itemAdded.notify({ item : item });
    }
}



//Controller For Airport List

function AirportListController(model, view) {
    this._model = model;
    this._view = view;

    var _this = this;
    
}

AirportListController.prototype = {
    addItem : function (airportName,airportCode) {
       
        var item = {
            'airport_name'  : airportName,
            'airport_code'  : airportCode
        }
        if (item) {
            this._model.addItem(item);
        }
    }
    
};

//View Class for airport list

function AirportListView(model,elements){
    this._model             = model;
    this._elements          = elements;

    this.listModified       = new Event(this);

    var _this               = this;
    
    this._model.itemAdded.attach(function () {
        _this.rebuildList();
    });
}

AirportListView.prototype = {
    show : function () {
        this.rebuildList();
    },

    rebuildList : function () {
        var list, items, key;

        list = this._elements.list;
        list.html('');

        items = this._model.getItems();
        
        var i =0;
        if(items.length > -1){
            for (i=0;i<items.length;i++) {
            
                var airport_item  = '<li class="airport_name"><a href="#" class="port_name">'+items[i].airport_name+'</a>';
                    airport_item += '    <a href="#" class="port_code">'+items[i].airport_code+'</a>';
                    airport_item += '    <div class="clear"></div>'
                    airport_item += '</li>';

                list.append(jQuery(airport_item));
            
            }
        }
        
    }
};
