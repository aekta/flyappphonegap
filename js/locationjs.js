
document.addEventListener("deviceready", CreateFileWriter, false);

function CreateFileWriter()
{
    // request the file system object
    window.requestFileSystem( LocalFileSystem.PERSISTENT, 0, OnFileSystemSuccess,fail);
}

function OnFileSystemSuccess( pFileSystemObj )
{
    console.log( pFileSystemObj.name );
    console.log( pFileSystemObj.root.name );
    alert(pFileSystemObj.name);
    alert(pFileSystemObj.root.name);
    pFileSystemObj.root.getFile( "file_name.txt", {create: true, exclusive: false}, OnFileGetSuccess, fail);
}

function OnFileGetSuccess( pFileEntryObj )
{
    alert("success");
    pFileEntryObj.createWriter( function(pWriterObj){ 
        var gWriterObj  = pWriterObj; 
        alert("filename:"+gWriterObj.filename);
         gWriterObj.onwrite = function(evt) {
            alert("write success");
        };
        gWriterObj.write("some sample text");
    }, fail );
}

function gotFileWriter(writer) {
    writer.onwriteend = function(evt) {
        console.log("contents of file now 'some sample text'");
        writer.truncate(11);
        writer.onwriteend = function(evt) {
            console.log("contents of file now 'some sample'");
            writer.seek(4);
            writer.write(" different text");
            writer.onwriteend = function(evt){
                console.log("contents of file now 'some different text'");
            }
        };
    };
    writer.write("some sample text");
}

function fail(evt)
{
    alert(evt.target.error.code);
    console.log(evt.target.error.code);
}

function readFile(){
    alert();
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
}
function gotFS(fileSystem) {
    fileSystem.root.getFile("file_name.txt", null, gotFileEntry, fail);
}

function gotFileEntry(fileEntry) {
    fileEntry.file(gotFile, fail);
}

function gotFile(file){
    readDataUrl(file);
    readAsText(file);
}

function readDataUrl(file) {
    var reader = new FileReader();
    reader.onloadend = function(evt) {
        console.log("Read as data URL");
        console.log(evt.target.result);
    };
    reader.readAsDataURL(file);
}

function readAsText(file) {
    var reader = new FileReader();
    reader.onloadend = function(evt) {
        console.log("Read as text");
        console.log(evt.target.result);
    };
    reader.readAsText(file);
    alert(reader.result);
}