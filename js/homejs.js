var weekday=new Array(7);
weekday[0]="Sunday";
weekday[1]="Monday";
weekday[2]="Tuesday";
weekday[3]="Wednesday";
weekday[4]="Thursday";
weekday[5]="Friday";
weekday[6]="Saturday";

var latitude;
var longitude;
var country_code;
var location_detail;
var country_info
jQuery(document).ready(function(){
   
   var onSuccess = function(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        if(location_detail == null){
            jQuery.ajax({
                type        : "GET",
                dataType    : "json",
                url         : jQuery.fn.flyApp.constant.googleapiurl + 'geocode/json', //"http://maps.googleapis.com/maps/api/geocode/json",
                data        : {
                    'latlng': latitude+","+longitude,
                    'sensor': false
                },
                success     : function(data){
                    if(data.results.length){
                        location_detail = data;
                        country_info        = jsonPath(location_detail.results[0],"$.address_components[?(@.types[0]=='country')]");
                        country_code        = country_info[0].short_name; 
                    }else{
                       // alert('invalid address');
                   }
                },error:function(x,e){
                   // alert("HU kam ni error" + x.status);
                },complete  :function(){
                   getAirportList(); 
                }
            });
        }

    } 
    // onError Callback receives a PositionError object
    //
    var onError = function onError(error) {
        if(error.code == null){
            alert('please try again');
        }else{
            alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
        }
        

    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError,{
        enableHighAccuracy  : true,
        timeout             : 60000,
        maximumAge          : 30000
    });
    
    

    //alert('hu navro emnem');

    function getAirportList(){

        var url = jQuery.fn.flyApp.constant.serverUrl + 'airports/airportList/' + country_code; //http://192.168.1.17/flying_app_services/airports/airportList/'+country_code;
        var model = new AirportList(),
        view      = new AirportListView(model, {
             'list'      : jQuery('#airport_list')
        }),
        controller = new AirportListController(model, view);
        jQuery.ajax({ 
            url: url,
            //type: "POST",
            dataType:"json",
            beforeSend:function(){
                jQuery('#loader').show();
                jQuery('#airport_list').hide();
            },
            success: function(data){
                //alert(data);
                //or you could iterate the components for only the city and state
                jQuery.each(data,function(i,airport){
                    controller.addItem(airport.name, airport.city_code);
                    view.show();
                });

            },
            error:function(x,e){
                alert("HU kam ni error" + x.status);
            },
            complete:function(){
                jQuery('#loader').hide();
                jQuery('#airport_list').show();
            }
        });
    }

  jQuery(document).delegate('.airport_name','click',function(){
    //alert('i m clicked');
     var url = jQuery.fn.flyApp.constant.flyappurl + 'weather/rest/v1/json/metar/'+jQuery(this).find('.port_code').text()+'?appId=17fddf0e&appKey=a1309f4d7918a38a58c9665c142a9557';

     jQuery.ajax({
        url : url,
        dataType:"json",
        success: function(data){
            //alert(' i m in success');
            loadWeatherDetail(data);
        },
        error:function(x,e){
           // alert("HU kam ni error" + x.status);
        },
        complete:function(data){

        }

     });

  });

});

function loadWeatherDetail(data){
    //alert('load weather is called');
    var dateTime        = '';
    var dataAvailable   = jsonPath(data, "$.appendix.airports[0]");
    if(dataAvailable != false){
        dateTime            = ""+jsonPath(data, "$.appendix.airports[0].localTime");

        var date            = (dateTime.slice(0, 10)).split("-",10);
        var name            = jsonPath(data, "$.appendix.airports[0].name");
        var place           = jsonPath(data, "$.appendix.airports[0].city")+","+jsonPath(data, "$.appendix.airports[0].countryCode");
        var time            = dateTime.slice(12, 16);
        var day             = weekday[(new Date(date)).getDay()];
        var windSpeed       = jsonPath(data, "$.metar.conditions.wind.speedKnots");
        var visibility      = jsonPath(data, "$.metar.conditions.visibility.miles");
        var skyCondition    = jsonPath(data, "$.metar.conditions.skyConditions[0].coverage");
        var tempCelsius     = jsonPath(data, "$.metar.temperatureCelsius");
        var tempFerenhit    = parseFloat(tempCelsius) + 33.8;
    }
    jQuery.ajax({
         url : 'airport_overview.html',
         success: function(data){
             if(dataAvailable != false){
                jQuery('#main_content').html(data);
                
                jQuery('#airport_fullname').text(name);
                jQuery('#main_content').find('.content').find('.data_not_available').hide();
                jQuery('#main_content').find('.content').find('.airport_details').show();
                jQuery('#city_country').text(place);
                jQuery('#day').text(day);
                jQuery('#time').text(time);
                jQuery('#current_date').text(date[2]+"/"+date[1]);
                jQuery('#skyCondition').text(skyCondition);
                jQuery('#temp_fer').text(""+parseInt(tempFerenhit)+"");
                jQuery('#temp_cel').text(tempCelsius+'°C');
                jQuery('#windSpeed').text(""+parseInt(windSpeed));
                jQuery('#visibility').text(""+parseInt(visibility));
             }else{
                jQuery('#main_content').html(data);
                jQuery('#main_content').find('.content').find('.airport_details').hide();
                jQuery('#main_content').find('.content').find('.data_not_available').show();
                jQuery('#main_content').find('.content').text('data not available');
             }
            
        },
        error:function(x,e){
           // alert("HU kam ni error" + x.status);
        }
     });
}
